//  ------------------------------------------------------------------------
//
//  Copyright (C) 2011-2012 Fons Adriaensen <fons@linuxaudio.org>
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//  ------------------------------------------------------------------------


#ifndef __LRXPROC_H
#define __LRXPROC_H


#include "global.h"
#include "lrxconf.h"
#include "lr4filt.h"
#include "delayproc.h"


class Lrxproc
{
public:

    Lrxproc (void);
    ~Lrxproc (void);

    int  init (Lrxconf *C, float fsamp);
    void clear (void);
    void reset (void);
    void process (int nfr, float *inp[], float *out[]);

private:

    void process2 (int nfr, float *inp[], float *out[]);
    void process3 (int nfr, float *inp[], float *out[]);
    void process4 (int nfr, float *inp[], float *out[]);
    void proctwin (int i, int j, int n, float ga, float gb, float *inp, float *outa, float *outb);

    int        _nchan;
    int        _nband;
    float      _fsamp;
    LR4_conf   _xconf [MAXFILT];
    LR4_filt  *_xover [MAXCHAN * MAXFILT];
    Delayproc *_delay [MAXCHAN * MAXBAND];
    float      _chan_gain [MAXCHAN];
    float      _band_gain [MAXBAND];
    float     *_buff1;
    float     *_buff2;
};    


#endif
