//  ------------------------------------------------------------------------
//
//  Copyright (C) 2011-2012 Fons Adriaensen <fons@linuxaudio.org>
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//  ------------------------------------------------------------------------


#ifndef __JCLIENT_H
#define __JCLIENT_H


#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>
#include <clthreads.h>
#include <jack/jack.h>
#include "lrxproc.h"


class Jclient : public A_thread
{
public:

    Jclient (const char *jname, const char *jserv);
    ~Jclient (void);

    const char *jname (void) const { return _jname; }
    int fsamp (void) const { return _fsamp; }

    int  init_lrxproc (Lrxconf *C);
    void create_inpports (Lrxconf *C); 
    void create_outports (Lrxconf *C); 
    void delete_outports (void); 
    void connect_outports (Lrxconf *C); 
    void disconn_outports (void); 

private:

    enum { ST_BYPASS, ST_SILENCE, ST_PROCESS };

    void  init_jack (const char *jname, const char *jserv);
    void  close_jack (void);
    void  jack_shutdown (void);
    int   jack_process (jack_nframes_t nframes);

    virtual void thr_main (void) {}

    jack_client_t  *_jack_client;
    jack_port_t    *_inpports [MAXCHAN];
    jack_port_t    *_outports [MAXCHAN * 4];
    const char     *_jname;
    unsigned int    _state;
    unsigned int    _fsamp;
    unsigned int    _fsize;
    int             _ninpp;
    int             _noutp;
    Lrxproc         _lrxproc;

    static void jack_static_shutdown (void *arg);
    static int  jack_static_process (jack_nframes_t nframes, void *arg);
};


#endif
