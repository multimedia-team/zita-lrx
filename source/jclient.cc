//  ------------------------------------------------------------------------
//
//  Copyright (C) 2011-2012 Fons Adriaensen <fons@linuxaudio.org>
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//  ------------------------------------------------------------------------


#include <string.h>
#include "jclient.h"


Jclient::Jclient (const char *jname, const char *jserv) :
    A_thread ("Jclient"),
    _jack_client (0),
    _jname (jname),
    _state (0),
    _fsamp (0),
    _fsize (0),
    _ninpp (0),
    _noutp (0)
{
    init_jack (jname, jserv);   
}


Jclient::~Jclient (void)
{
    if (_jack_client) close_jack ();
}


void Jclient::init_jack (const char *jname, const char *jserv)
{
    jack_status_t  stat;
    int            opts;

    opts = JackNoStartServer;
    if (jserv) opts |= JackServerName;
    if ((_jack_client = jack_client_open (jname, (jack_options_t) opts, &stat, jserv)) == 0)
    {
        fprintf (stderr, "Can't connect to JACK\n");
        exit (1);
    }

    jack_set_process_callback (_jack_client, jack_static_process, (void *) this);
    jack_on_shutdown (_jack_client, jack_static_shutdown, (void *) this);

    if (jack_activate (_jack_client))
    {
        fprintf(stderr, "Can't activate JACK.\n");
        exit (1);
    }

    _jname = jack_get_client_name (_jack_client);
    _fsamp = jack_get_sample_rate (_jack_client);
    _fsize = jack_get_buffer_size (_jack_client);
    _state = ST_BYPASS;
}


void Jclient::close_jack ()
{
    jack_deactivate (_jack_client);
    jack_client_close (_jack_client);
}


void Jclient::jack_static_shutdown (void *arg)
{
    return ((Jclient *) arg)->jack_shutdown ();
}


void Jclient::jack_shutdown (void)
{
    send_event (EV_SHUTDOWN, 1);
}


int Jclient::jack_static_process (jack_nframes_t nframes, void *arg)
{
    return ((Jclient *) arg)->jack_process (nframes);
}


int Jclient::jack_process (jack_nframes_t nframes)
{
    int   ev, i;
    float *ipp [MAXCHAN];
    float *opp [MAXCHAN * MAXBAND];

    ev = get_event_nowait ();
    if (ev != EV_TIME) switch (ev)
    {
    case EV_GO_BYPASS:  _state = ST_BYPASS;  break;
    case EV_GO_SILENCE: _state = ST_SILENCE; break;
    case EV_GO_PROCESS: _state = ST_PROCESS; break;
    }

    if (_state != ST_BYPASS)
    {
        for (i = 0; i < _noutp; i++) opp [i] = (float *) jack_port_get_buffer (_outports [i], nframes);
	if (_state == ST_PROCESS)
	{
  	    for (i = 0; i < _ninpp; i++) ipp [i] = (float *) jack_port_get_buffer (_inpports [i], nframes);	 
  	    _lrxproc.process (nframes, ipp, opp);
	}
	else
	{
	    for (i = 0; i < _noutp; i++) memset (opp [i], 0, nframes * sizeof (float));
	}
    }

    if (ev != EV_TIME) send_event (ev, 1);
    return 0;
}


int Jclient::init_lrxproc (Lrxconf *C)
{
    return _lrxproc.init (C, _fsamp);
}


void Jclient::create_inpports (Lrxconf *C)
{
    int  i;
    char s [64];

    for (i = 0; i < C->_nchan; i++)
    {
 	snprintf (s, 64, "inp-%s", C->_chann [i]._label);
        _inpports [i] = jack_port_register (_jack_client, s, JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
    }
    _ninpp = C->_nchan;
}


void Jclient::create_outports (Lrxconf *C)
{
    int  i, j;
    char s [64];

    delete_outports ();
    for (i = 0; i < C->_nchan; i++)
    {
	for (j = 0; j < C->_nband; j++)
	{
 	    snprintf (s, 64, "out-%s.%s", C->_chann [i]._label, C->_fband [j]._label);
            _outports [_noutp++] = jack_port_register (_jack_client, s, JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
        }
    }
}


void Jclient::delete_outports (void)
{
    for (int i = 0; i < _noutp; i++)
    {
	jack_port_unregister (_jack_client, _outports [i]);
    }
    _noutp = 0;
}


void Jclient::connect_outports (Lrxconf *C)
{
    int i, j;
    char s [64];

    for (i = 0; i < C->_nchan; i++)
    {
	for (j = 0; j < C->_nband; j++)
	{
	    if (C->_chann [i]._dest [j])
	    {
	        snprintf (s, 64, "%s:out-%s.%s", _jname, C->_chann [i]._label, C->_fband [j]._label);
	        jack_connect (_jack_client, s, C->_chann [i]._dest [j]);
	    }
        }
    }
}


void Jclient::disconn_outports (void)
{
    for (int i = 0; i < _noutp; i++)
    {
	jack_port_disconnect (_jack_client, _outports [i]);
    }
}


