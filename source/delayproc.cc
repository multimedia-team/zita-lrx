//  ------------------------------------------------------------------------
//
//  Copyright (C) 2011-2012 Fons Adriaensen <fons@linuxaudio.org>
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//  ------------------------------------------------------------------------


#include <stdio.h>
#include <string.h>
#include "delayproc.h"


Delayproc::Delayproc (int maxdel, int period)
{
    int k;

    k = (maxdel + period - 1) / period;
    _length = period * (k + 1);
    _period = period;
    _data = new float [_length + _period];
    _wrind = 0;
    _rdind = 0;
    reset ();
}


Delayproc::~Delayproc (void)
{
    delete[] _data;
}


void Delayproc::reset (void)
{
    memset (_data, 0, (_length + _period) * sizeof (float));
}
