Configuration file format for zita-lrx
--------------------------------------

See the files in the directory 'examples' for some
examples. Lines starting with '#' are comments.

Channel, crossover and frequency band indices all start at 1.
The following commands are available:


/zita-lrx  <channels> <frequency bands>

    This command must be the first one. Channels can be 1..16, 
    frequency bands can be 2..4.


/xover/freq  <index> <frequency>

    Crossover frequencies in Hz. These must be in rising order
    when sorted on index.	   


/xover/shape  <index> <shape>

    Shape sets the gain in dB at the crossover frequency and can
    be from -6 (Linkwitz-Riley) to -3 (Butterworth). The default
    is -6 dB.


/chann/label  <index> <name>

    Channel name used in Jack port names, up to 15 characters.
    The default channel names are 1..n.


/chann/gain   <index> <gain>

    Per-channel gain in dB. The range is -30..+10 dB.	      


/chann/delay   <index> <gain>

    Per-channel delay in milliseconds. The range is 0..30 ms.	      


/chann/connect <channel index> <freq band index> <destination port>

    Connect selected output to destination port. Only one connection
    per output is supported.	      


/fband/suffix  <index> <suffix>

    Per frequency band suffix used in output port names, up to 15 char.
    The default suffixes are 1..n.


/fband/gain  <index> <gain>
    
    Per frequency band gain in dB, applied to all channels. The range 
    is -30..+10 dB.


/fband/delay  <index> <delay>

    Per frequency band delay in milliseconds, applied to all channels.
    Range is 0..30 ms.



All commands except /zita-lrx and /xover/freq are optional.


-- 
FA
